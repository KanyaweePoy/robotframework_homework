*** Settings ***
Library  Builin

*** Variables ***
${area}=  0
${pi}=  3.14
${radius}=  4 * 4

*** Test Cases ***
1. คำนวณพื้นที่วงกลม
    ${area}=  CircleArea  ${pi}  ${radius}
    Set Global Variable  ${area}

2. แสดงผล
    Log To Console  \nArea: ${area}

*** Keywords ***
CircleArea
    [Arguments]  ${paraPi}  ${paraRadius}
    ${respArea}=  Evaluate  ${paraPi} * ${paraRadius}
    [Return]  ${respArea}
