*** Settings ***
Library  BuiltIn
Library  SeleniumLibrary

*** Variables ***
${url}=  https://ptt-devpool-robotframework.herokuapp.com
${id}=  admin
${pass}=  1234

&{Dictionary}=  f_name= Amanda  l_name= Person  email= amanda.p@gmail.com
&{Dictionary1}=  f_name= Jane  l_name= Doe  email= jane.d@gmail.com
&{Dictionary2}=  f_name= Janet  l_name= Doent  email= janet.d@gmail.com
&{Dictionary3}=  f_name= George  l_name= Person  email= george.p@gmail.com

@{listmydict}=  Dictionary  Dictionary1  Dictionary2  Dictionary3

*** Test Cases  ***
1. เปิด Browser
    Open Browser  about:blank  browser= Chrome
    Maximize Browser Window
    Set Selenium Speed  0.5

2. ไปที่เว็บไซต์ https://ptt-devpool-robotframework.herokuapp.com
    Go To  ${url}

3. กรอกข้อมูล เพื่อ Login
    Input Text  id= txt_username  ${id}
    Input Text  id= txt_password  ${pass}

4. กดปุ่มค้นหา
    Click Button  id= btn_login

5. กดปุ่ม New Data
    Click Button  id= btn_new

6. เพิ่มข้อมูลลงฟอร์ม
    FOR  ${i}  IN  @{listmydict}
        Test  &{${i}}
        Click Button  id= btn_new
    END
    Click Button  id= btn_new_close

7. Screenshot
    Capture Page Screenshot  filename=${CURDIR}/screenshot.png


*** Keywords ***
Test
    [Arguments]    &{para}
    FOR    ${key}  ${value}   IN    &{para}
            Run Keyword If  '${key}' == 'f_name'   Input Text  id=txt_new_firstname  ${para["${key}"]}
            Run Keyword If  '${key}' == 'l_name'   Input Text  id=txt_new_lastname  ${para["${key}"]}
            Run Keyword If  '${key}' == 'email'   Input Text  id=txt_new_email  ${para["${key}"]}
    END
    Click Button  id= btn_new_save
    